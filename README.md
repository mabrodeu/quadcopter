#quadcopter

##Quadcopter visualisation processing

##How to test:

1) Put the demo file (gyro.html) on a local server running on your machine. (Ex: WAMP, MAMP...) On a Mac you can also open the terminal and navigate to the gyro.html file directory and then enter the following command : 
```
python -m SimpleHTTPServer 8081
```
This will start a simple http server on your local machine on port 8081. You can access the folder by going to http://localhost:8081, or on a device on the same network using: http://(server local ip):8081

2) Once the demo file is available from your mobile device (http://(server local ip):8081), you can now run the processing file (quadcopter.pde).

3) In the console, you should see "Server done". This means the websocket processing server is running.

4) On your mobile device, make sure the screen rotation is locked so it will not flip when you move the device. Put the device down on a table or align it to the initial position.

5) Refresh the page on your mobile device. You should now see the accelerometer values on the page. The cube in the processing sketch should also now be moving as you move your mobile device!