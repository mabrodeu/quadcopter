/**
* Quadcopter. 
* 
* Description
*/

import muthesius.net.*;
import org.webbitserver.*;

WebSocketP5 socket;
String tabOrientations[] = new String[3];

int currentID = 0;


void setup() {
	size(1280, 720, P3D);
	socket = new WebSocketP5(this,8080);

	for(int i=0; i<tabOrientations.length; i++){
		tabOrientations[i] = "0";
	}
	
}

void draw() {
	background(0);

	lights();

	pushMatrix();
		stroke(0);
		fill(200);
		translate(width/2, height/2, 0);

		rotateX(radians(parseFloat(tabOrientations[1])));
		rotateY(-radians(parseFloat(tabOrientations[0])));
		rotateZ(-radians(parseFloat(tabOrientations[2])));
		box(300, 20, 300);
	popMatrix();


	pushMatrix();
		stroke(0);
		fill(0,255,0);
		translate(width/2, height, 0);
		box(width*2, 20, width*2);
	popMatrix();
	
}

void stop(){
	socket.stop();
}

void websocketOnMessage(WebSocketConnection con, String msg){
	if(msg.equals("Ping")){
		socket.broadcast("pong");
	} else{
		
		//println(msg);
		try {
			
			tabOrientations = split(msg, ",");

		} catch (Exception e) {
			
		}
		
	} 
}

void websocketOnOpen(WebSocketConnection con){
  println("A client joined");
  //con.send("Ton id : " + currentID);
  //currentID++;
 


}

void websocketOnClosed(WebSocketConnection con){
  println("A client left");
}